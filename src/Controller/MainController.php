<?php

namespace App\Controller;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    const API_BASE_URL = "https://api.stlouisfed.org";
    const ENDPOINT = "/fred/category/series";

    /**
     * @Route("/", name="main")
     */
    public function main()
    {
        $apiKey = $_ENV['API_KEY'];

        $data = (new Client())->request('GET', self::API_BASE_URL . self::ENDPOINT, [
            'query' => [
                'api_key' => $apiKey,
                'file_type' => 'json',
                'category_id' => '125'
            ]
        ])->getBody()->getContents();

        return $this->render('main.html.twig', ['series' => json_decode($data)->seriess]);
    }
}